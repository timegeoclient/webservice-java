package geoclient;

import java.util.List;

import model.dao.ColaboradorDAO;
import model.dao.EmpresaDAO;
import model.dao.LocalizacaoDAO;
import solution.Colaborador;

public class ColaboradorTest {
	public static void main(String[] args) {
		//ColaboradorDAO colaboradorDAO = new ColaboradorDAO();		
		//colaboradorDAO.save(new Colaborador("098890", "colabX@gmail.com", "432765", "Colaborador 2",
//			"6543", "098765", "Android", "colabXY", true, "1.2", new EmpresaDAO().find(1)));
		//System.out.println(colaboradorDAO.searchByUsuarioSenha("colabX", "432123"));
		ColaboradorDAO colaboradorDAO = new ColaboradorDAO();
    	List<Colaborador> colaboradores = colaboradorDAO.all();
    	for (Colaborador colaborador : colaboradores) {
			System.out.println(colaborador);
		}
	}
}
