package geoclient;

import java.util.List;

import model.dao.ColaboradorDAO;
import model.dao.EmpresaDAO;
import model.dao.LocalizacaoDAO;
import solution.Colaborador;
import solution.Localizacao;

public class LocalizacaoTest {
	public static void main(String[] args) {
		LocalizacaoDAO localizacaoDAO = new LocalizacaoDAO();
		Colaborador colaborador = new ColaboradorDAO().find(2);
		Localizacao localizacao = new Localizacao("bairro cliente", "cep", "cidadeCliente", 
													"complemento cliente", "CE", "lat", 
													"logradouro", "long", "nome", "numero", 
													"precisao", "rua", 1, "PENDENTE", 
													new EmpresaDAO().find(1), colaborador);
		//localizacaoDAO.save(localizacao);				
    	List<Localizacao> localizacoes = localizacaoDAO.getByColaborador(colaborador);
    	for (Localizacao localizacao2 : localizacoes) {
			System.out.println(localizacao2.getId());
		}
	}
}
