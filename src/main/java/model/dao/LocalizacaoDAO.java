package model.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import solution.Colaborador;
import solution.Localizacao;

public class LocalizacaoDAO extends DAO{
	public void save(Localizacao localizacao){
		super.save(localizacao);
    }
	
	@SuppressWarnings("unchecked")
	public List<Localizacao> all(){
		return (List<Localizacao>) super.all("Localizacao");
    }
	
	public List<Localizacao> getByColaborador(Colaborador colaborador){
		super.open();		
		CriteriaBuilder builder = super.getEntityManager().getCriteriaBuilder();		
		CriteriaQuery<Localizacao> criteriaQuery = builder.createQuery(Localizacao.class);		
		Root<Localizacao> localizacao = criteriaQuery.from(Localizacao.class);
		TypedQuery<Localizacao> query = super.getEntityManager().createQuery(
												criteriaQuery.select(localizacao)
													.where(builder.equal(localizacao.get("colaborador"), colaborador)));				
		List<Localizacao> list = query.getResultList();
		super.close();
		return list;
	}
}
