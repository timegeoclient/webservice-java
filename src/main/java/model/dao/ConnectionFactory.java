package model.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.cfg.Configuration;

public class ConnectionFactory {
	public static EntityManagerFactory getConnection() {								
        return Persistence.createEntityManagerFactory("webservice");
    }
}
