package model.dao;

import solution.Empresa;

public class EmpresaDAO extends DAO{
		
	public void save(Empresa empresa){		
		super.save(empresa);					
    }
	
	public Empresa find(int id){		
		return (Empresa) super.find(Empresa.class, id);
    }
	
}
