package model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

public abstract class DAO {
	private static EntityManagerFactory entityManagerFactory;	
	private EntityManager entityManager;	

	public DAO(){
		this.setEntityManagerFactory(ConnectionFactory.getConnection());
	}

    protected void open(){    	
    	entityManager = entityManagerFactory.createEntityManager();    	
    }
    
    protected void close(){
    	entityManager.close();
    }    
        
    protected final void save(Object object){
    	if (object != null){    	
	    	this.open();
	    	
	    	entityManager.getTransaction().begin();
	    	entityManager.persist(object);
	    	entityManager.getTransaction().commit();
	    	this.close();    	
    	}else
    		throw new NullPointerException();
    }
        
    protected Object find(Class<?> entityClass, int id){
    	this.open();
    	entityManager.getTransaction().begin();
    	Object object = entityManager.find(entityClass, id);    	
    	this.close();
    	return object;
    }
        
    protected List<?> all(String tableName){
    	this.open();
    	entityManager.getTransaction().begin();
    	List<?> list = entityManager.createQuery("from "+tableName).getResultList();
    	this.close();
    	return list;
    }
    
	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
        
}
