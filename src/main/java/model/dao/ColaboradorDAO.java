package model.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import solution.Colaborador;

public class ColaboradorDAO extends DAO {
	
	public void save(Colaborador colaborador){		
		super.save(colaborador);					
    }
	
	public Colaborador find(int id){		
		return (Colaborador) super.find(Colaborador.class, id);
    }
	
	@SuppressWarnings("unchecked")
	public List<Colaborador> all(){		
		return (List<Colaborador>) super.all("Colaborador");
    }
	
	public Colaborador searchByUsuarioSenha(String usuario, String senha){
		super.open();
		CriteriaBuilder builder = super.getEntityManager().getCriteriaBuilder();		
		CriteriaQuery<Colaborador> criteriaQuery = builder.createQuery(Colaborador.class);		
		Root<Colaborador> colaborador = criteriaQuery.from(Colaborador.class);
		TypedQuery<Colaborador> query = super.getEntityManager().createQuery(
												criteriaQuery.select(colaborador)
													.where(builder.equal(colaborador.get("usuario"), usuario))
													.where(builder.equal(colaborador.get("senha"), senha)));
		List<Colaborador> colaboradores = query.getResultList();
		this.close();
		if (colaboradores.size() == 1)
			return colaboradores.get(0); 		    	
    	return null;
	}
}
