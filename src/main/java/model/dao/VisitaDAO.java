package model.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import solution.Colaborador;
import solution.Visitas;

public class VisitaDAO extends DAO {
	
	public void save(Visitas visita){
		super.save(visita);
	}
	
	@SuppressWarnings("unchecked")
	public List<Visitas> all(){
		return (List<Visitas>) super.all("Visitas");
	}
	
	public List<Visitas> getByColaborador(Colaborador colaborador){
		super.open();		
		CriteriaBuilder builder = super.getEntityManager().getCriteriaBuilder();		
		CriteriaQuery<Visitas> criteriaQuery = builder.createQuery(Visitas.class);		
		Root<Visitas> visitas = criteriaQuery.from(Visitas.class);
		TypedQuery<Visitas> query = super.getEntityManager().createQuery(
												criteriaQuery.select(visitas)
													.where(builder.equal(visitas.get("colaborador"), colaborador)));				
		List<Visitas> list = query.getResultList();
		super.close();
		return list;
	}
	
}
