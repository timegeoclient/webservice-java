package model.enums;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum EnumMensagens {	
	
	USUARIO_SENHA_COLABORADOR_INVALIDOS(1, "Usuário/senha do colaborador inválido(s)."),
	USUARIO_SENHA_COLABORADOR_VALIDOS(2, "Usuário e senha do colaborador válidos.");	
	
	private int codigo_erro;
	private String descricao;
	
	private EnumMensagens(int codigo_erro, String descricao) {
		this.codigo_erro = codigo_erro;
		this.descricao = descricao;
	}
	
	public int getCodigo_erro() {
		return codigo_erro;
	}
	
	public void setCodigo_erro(int codigo_erro) {
		this.codigo_erro = codigo_erro;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
