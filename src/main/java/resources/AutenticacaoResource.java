package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.dao.ColaboradorDAO;
import model.enums.EnumMensagens;
import solution.Colaborador;

@Path("autenticacao")
public class AutenticacaoResource {
	
	private Colaborador currentColaborador;
	
	@POST
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes("application/x-www-form-urlencoded")
	public EnumMensagens autenticate(@FormParam("usuario") String usuario, @FormParam("senha")String senha) {
    	if (usuario != null && senha != null && !usuario.isEmpty() && !senha.isEmpty()){
    		Colaborador colaborador = new ColaboradorDAO().searchByUsuarioSenha(usuario, senha);
    		if (colaborador != null){
    			this.setCurrentColaborador(colaborador);
    			return EnumMensagens.USUARIO_SENHA_COLABORADOR_VALIDOS;
    		}
    	}
    	return EnumMensagens.USUARIO_SENHA_COLABORADOR_INVALIDOS;
    }

	public Colaborador getCurrentColaborador() {
		return currentColaborador;
	}

	public void setCurrentColaborador(Colaborador currentColaborador) {
		this.currentColaborador = currentColaborador;
	}
	
}
