package resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.dao.VisitaDAO;
import model.enums.EnumMensagens;
import solution.Colaborador;
import solution.Visitas;

@Path("visita")
public class VisitasResource {
	
    @POST
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes("application/x-www-form-urlencoded")
    public List<Visitas> getAll(@FormParam("usuario") String usuario, @FormParam("senha") String senha) {
    	AutenticacaoResource autenticacaoResource = new AutenticacaoResource();
    	EnumMensagens enumMensagens = autenticacaoResource.autenticate(usuario, senha);
    	if (enumMensagens.equals(EnumMensagens.USUARIO_SENHA_COLABORADOR_VALIDOS)){
    		Colaborador colaborador = autenticacaoResource.getCurrentColaborador();
	    	VisitaDAO visitaDAO = new VisitaDAO();
	    	List<Visitas> visitasDoBanco = visitaDAO.getByColaborador(colaborador),
	    						visitasEmJson = new ArrayList<Visitas> (visitasDoBanco.size());    	
	    	for (int i = visitasDoBanco.size() - 1; i >= 0; i--) {
	    		Visitas visitaDoBanco = visitasDoBanco.get(i);
				visitasEmJson.add(new Visitas(visitaDoBanco.getId(), visitaDoBanco.getBairro(), visitaDoBanco.getCidade(), 
											visitaDoBanco.getDataAprovacao(), visitaDoBanco.getDataAtribuicao(), visitaDoBanco.getDataRetorno(), 
											visitaDoBanco.getDataRejeicao(), visitaDoBanco.getLatitude(), visitaDoBanco.getLogradouro(), 
											visitaDoBanco.getLongitude(), visitaDoBanco.getNumero(), visitaDoBanco.getPrecisao(), 
											visitaDoBanco.getRua(), visitaDoBanco.getNumeroSatelitesAtivos(), visitaDoBanco.getStatus(), 
											visitaDoBanco.getFeita()));
			}
	    	return visitasEmJson;
    	}
    	return null;
    	
    }
}
