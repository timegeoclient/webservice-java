package resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.dao.LocalizacaoDAO;
import model.enums.EnumMensagens;
import solution.Colaborador;
import solution.Localizacao;

@Path("localizacao")
public class LocalizacaoResource {
	/**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    //@Produces("application/json;charset=utf-8")
    public List<Localizacao> getAll(@FormParam("usuario") String usuario, @FormParam("senha") String senha) {
    	AutenticacaoResource autenticacaoResource = new AutenticacaoResource();
    	EnumMensagens enumMensagens = autenticacaoResource.autenticate(usuario, senha);
    	if (enumMensagens.equals(EnumMensagens.USUARIO_SENHA_COLABORADOR_VALIDOS)){
    		Colaborador colaborador = autenticacaoResource.getCurrentColaborador();
	    	LocalizacaoDAO localizacaoDAO = new LocalizacaoDAO();
	    	List<Localizacao> localizacoesDoBanco = localizacaoDAO.getByColaborador(colaborador),
	    						localizacoesEmJson = new ArrayList<Localizacao> (localizacoesDoBanco.size());    	
	    	for (int i = localizacoesDoBanco.size() - 1; i >= 0; i--) {
				Localizacao localizacaoDoBanco = localizacoesDoBanco.get(i);
				localizacoesEmJson.add(new Localizacao(localizacaoDoBanco.getId(), localizacaoDoBanco.getBairro(), localizacaoDoBanco.getCep(), 
														localizacaoDoBanco.getCidade(), localizacaoDoBanco.getComplemento(), 
														localizacaoDoBanco.getEstado(), localizacaoDoBanco.getLatitude(), 
														localizacaoDoBanco.getLogradouro(), localizacaoDoBanco.getLongitude(), 
														localizacaoDoBanco.getNome(), localizacaoDoBanco.getNumero(), 
														localizacaoDoBanco.getPrecisao(), localizacaoDoBanco.getRua(), 
														localizacaoDoBanco.getNumeroSatelitesAtivos(), localizacaoDoBanco.getStatus(), 
														null, null, null));
			}
	    	return localizacoesEmJson;
    	}
    	return null;
    }
}
